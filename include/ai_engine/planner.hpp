#ifndef PLANNER_HPP_
#define PLANNER_HPP_

//Much credit goes to Gilwoo Lee and the personal robotics lab for writing
//this code. We are simply reformating.

#include <aikido/constraint/NonColliding.hpp>
#include <aikido/constraint/TSR.hpp>
#include <aikido/statespace/dart/MetaSkeletonStateSpace.hpp>
#include <aikido/trajectory/Interpolated.hpp>
#include <aikido/trajectory/Trajectory.hpp>
#include <dart/dart.hpp>
#include <memory>

namespace planner {

class Planner
{
public:
     Planner(dart::dynamics::SkeletonPtr robot, std::vector<std::string> disableCollisions, double mCollisionResolution);

      aikido::trajectory::InterpolatedPtr planToConfiguration(
          aikido::statespace::dart::MetaSkeletonStateSpacePtr _space,
          const Eigen::VectorXd& _goal,
          double _timelimit,
          aikido::constraint::NonCollidingPtr _nonColliding) const;

      //used for TSR vector 
      aikido::trajectory:: InterpolatedPtr planToMultiTSR(
          aikido::statespace::dart::MetaSkeletonStateSpacePtr _space,
          dart::dynamics::BodyNodePtr _bn,
          const std::vector<aikido::constraint::TSR>& _tsr,
          int _maxNumTrials,
          double _timelimit,
          aikido::constraint::NonCollidingPtr _nonColliding) const;


      /// Plan the configuration of the metakeleton such that
      /// the specified bodynode is set to a sample in TSR
      /// \param _space The StateSpace for the metaskeleton
      /// \param _body Bodynode whose frame is meant for TSR
      /// \param _maxNumTrials Max number of trials for solving for IK
      /// \param _timelimit Max time (seconds) to spend per planning to each IK
      /// \param _nonColliding NonColliding constriant to check. Self-collision
      /// is checked by default.
      /// \return Trajectory to a sample in TSR, or nullptr if planning fails.
      aikido::trajectory::InterpolatedPtr planToTSR(
          aikido::statespace::dart::MetaSkeletonStateSpacePtr _space,
          dart::dynamics::BodyNodePtr _bn,
          const aikido::constraint::TSR& _tsr,
          int _maxNumTrials, double _timelimit,
          aikido::constraint::NonCollidingPtr _nonColliding) const;


protected:
      aikido::constraint::NonCollidingPtr getSelfCollisionConstraint(
          aikido::statespace::dart::MetaSkeletonStateSpacePtr _space) const;

private:
  dart::dynamics::SkeletonPtr _robot;
  std::vector<std::string> _disableCollisions;
  double _mCollisionResolution;

};

}  // ns

#endif  //PLANNER_HPP_
