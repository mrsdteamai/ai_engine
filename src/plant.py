import numpy as np

class PID(object):
    def __init__(self, gains):
        self.P    = np.array(gains['P'])
        self.I    = np.array(gains['I'])
        self.D    = np.array(gains['D'])
        self.dims = len(gains['P'])

        self.cum_error = np.zeros((self.dims,))
        self.old_error = np.zeros((self.dims,))
        self.lim_error = np.ones((self.dims,))

    def update(self, setpoint, state):
        error = np.array(setpoint) - np.array(state)

        p_error = error
        i_error = error + self.cum_error 
        d_error = error - self.old_error

        signal = self.P * p_error + \
                 self.I * i_error + \
                 self.D * d_error

        self.cum_error = np.min(np.hstack((np.max(np.hstack((
                                                             self.cum_error + error, 
                                                             -self.lim_error))), 
                                           self.lim_error)))
        if type(self.cum_error) is not np.ndarray:
            self.cum_error = np.array([self.cum_error])
        self.old_error = error

        return signal

class PIDPlant(object):
    def __init__(self, gains, rest=None):
        self.pid  = PID(gains)
        self.dims = len(gains['P'])

        self.eps  = 5e-2

        self.reset(rest)

    def reset(self, rest=None):
        if rest is not None and len(rest) == self.dims:
            self._state = np.array(rest)
        else:
            self._state = np.array([0.] * self.dims)

        self._setpoint = np.array(self._state)

    def setpoint(self, setpoint=None):
        if setpoint is not None and len(setpoint) == self.dims:
            self._setpoint = np.array(setpoint)

        return self._setpoint.tolist()

    def state(self):
        return self._state.tolist()

    def update(self):
        self._state += self.pid.update(self._setpoint, self._state)

    def error(self):
        return np.linalg.norm(self._setpoint - self._state)

    def ready(self):
        return self.error() < self.eps