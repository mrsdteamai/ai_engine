#!/usr/bin/env python

import sys
import time
import yaml
from itertools import count as Count

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from ai_engine.msg import JointTrajectory

class TestTrajectory(object):
    def __init__(self, base, filename):
        self.filename = filename
        self.base     = base

        self.joints = rospy.get_param(self.base)
        self.count  = Count()

        self.trajectory_pub = rospy.Publisher('/'+self.base+'/trajectory', JointTrajectory, queue_size=10)
        self.status_pub     = rospy.Publisher('/'+self.base+'/ttraj_status',     String,          queue_size=10)

        time.sleep(0.2)

    def run(self):

        rate = rospy.Rate(1)

        while not rospy.is_shutdown():
            status = "%s is alive at %s" % (self.base, rospy.get_time())
            
            #rospy.loginfo(status)

            self.status_pub.publish(status)

            trajectory = JointTrajectory()

            trajectory.waypoints = []

            with open(self.filename, 'r') as stream:
                waypoints = yaml.load(stream)

            for waypoint in waypoints:
                joints = JointState()

                joints.header.seq   = next(self.count)
                joints.header.stamp = rospy.Time.now()
                joints.name         = self.joints['name']
                joints.position     = waypoint

                trajectory.waypoints.append(joints)
                
            self.trajectory_pub.publish(trajectory)

            status = "%s sent trajectory from %s at %s" % (self.base, self.filename, rospy.get_time())

            #print trajectory

            rospy.loginfo(status)
            print len(trajectory.waypoints)

            #rate.sleep()

            break
            
        rospy.spin() #rate.sleep()


if __name__ == '__main__':
    rospy.init_node('test_trajectory', anonymous=True)

    # rosrun ai_engine test_trajectory ABB140 scr/ai_environment/config/ABB140_test_trajectory.yaml

    try:
        base     = sys.argv[1]
        filename = sys.argv[2]

        ttraj = TestTrajectory(base, filename)

        ttraj.run()

    except rospy.ROSInterruptException:
        pass