#!/usr/bin/env python

import sys
import time
from itertools import count as Count

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from actionlib import SimpleActionClient as Client
import ai_engine.msg

class TrajectoryExecutor(object):
    def __init__(self, name, base):
        self.name = name
        self.base = base

        self.joints = rospy.get_param(self.base)
        self.count  = Count()

        self.status_pub   = rospy.Publisher (self.base+'/texec_status', String,     queue_size=10)

        status = "%s starting client at %s/\'ctrl\'" % (self.base, self.base)

        #rospy.loginfo(status)

        self.client = Client(self.base+'/ctrl', 
                             ai_engine.msg.joints_waypointAction)

        status = "%s starting server at %s/\'exec\'" % (self.base, self.base)

        #rospy.loginfo(status)

        self.server = Server(self.base+'/exec', 
                             ai_engine.msg.joints_trajectoryAction, 
                             execute_cb=self.execute_callback,
                             auto_start=False)

        self.rate = 2

    def run(self):
        rate = rospy.Rate(self.rate)

        while not rospy.is_shutdown():
            status = "%s is alive at %s" % (self.base, rospy.get_time())
            
            #rospy.loginfo(status)

            self.status_pub.publish(status)

            ready = self.client.wait_for_server(rospy.Duration(0.1))

            if ready:
                joints = JointState()

                joints.header.seq      = next(self.count)
                joints.header.stamp    = rospy.Time.now()
                joints.header.frame_id = ''
                joints.name            = self.joints['name']
                joints.position        = [0.1*(joints.header.seq%5)]*len(self.joints['rest'])

                goal = ai_engine.msg.joints_waypointGoal(joints=joints)

                status = "%s sent goal %s at %s" % (self.base, goal, rospy.get_time())

                #rospy.loginfo(status)

                self.client.send_goal(goal)  
                
                self.client.wait_for_result()

                result = self.client.get_result()

                status = "%s received result %s at %s" % (self.base, result, rospy.get_time())

                #rospy.loginfo(status)

            rate.sleep()

    def execute_callback(self, goal):
        pass

if __name__ == '__main__':
    rospy.init_node('trajectory_executor', anonymous=True)

    try:
        name = rospy.get_name()
        base = sys.argv[1]

        texec = TrajectoryExecutor(name, base)

        texec.run()

        rospy.spin()

    except rospy.ROSInterruptException:
        pass