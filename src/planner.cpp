//Much credit goes to the personal robotics lab for writing
//this code. We are simply reformating.

//receives vector of TSR objects

#include <ai_engine/planner.hpp>

#include <random>
#include <stdexcept>
#include <string>
#include <chrono>

#include <Eigen/Core>
#include <aikido/constraint/JointStateSpaceHelpers.hpp>
#include <aikido/constraint/InverseKinematicsSampleable.hpp>
#include <aikido/constraint/FiniteSampleable.hpp>
#include <aikido/constraint/CyclicSampleable.hpp>
#include <aikido/constraint/Testable.hpp>
#include <aikido/constraint/TestableIntersection.hpp>
#include <aikido/distance/defaults.hpp>
#include <aikido/planner/ompl/CRRTConnect.hpp>
#include <aikido/planner/ompl/Planner.hpp>
#include <aikido/statespace/GeodesicInterpolator.hpp>
#include <aikido/statespace/dart/MetaSkeletonStateSpace.hpp>
#include <aikido/util/CatkinResourceRetriever.hpp>
#include <aikido/util/RNG.hpp>
#include <dart/utils/urdf/DartLoader.hpp>
#include <ompl/geometric/planners/rrt/RRTConnect.h>

#include <aikido/planner/SnapPlanner.hpp>

namespace planner {

using aikido::statespace::dart::MetaSkeletonStateSpacePtr;
using aikido::trajectory::InterpolatedPtr;
using aikido::constraint::NonCollidingPtr;
using dart::common::make_unique;
using dart::dynamics::BodyNodePtr;
using dart::dynamics::ChainPtr;
using dart::dynamics::MetaSkeleton;
using dart::dynamics::SkeletonPtr;

Planner::Planner(dart::dynamics::SkeletonPtr robot,  std::vector<std::string> disableCollisions, double mCollisionResolution)
{
    const dart::common::Uri herbUri{
    "package://herb_description/robots/herb.urdf"};
  using dart::dynamics::InverseKinematics;
  using dart::dynamics::Chain;
  dart::utils::DartLoader urdfLoader;
 
  // Resolves package:// URIs by emulating the behavior of 'catkin_find'.
  auto resourceRetriever =
    std::make_shared<aikido::util::CatkinResourceRetriever>();
  this->_robot = robot;
  //   // TODO: Load this from HERB's SRDF file.
  // std::vector<std::string> disableCollisions{"/left/hand_base",
  //                                            "/right/hand_base",
  //                                            "/left/wam1",
  //                                            "/right/wam1",
  //                                            "/left/wam6",
  //                                            "/right/wam6"};
  this->_disableCollisions = disableCollisions;
  this->_mCollisionResolution = mCollisionResolution;
};

InterpolatedPtr Planner::planToMultiTSR(MetaSkeletonStateSpacePtr _space,
  dart::dynamics::BodyNodePtr _bn,
  const std::vector<aikido::constraint::TSR>& _tsr,
  int _maxNumTrials,
  double _timelimit,
  aikido::constraint::NonCollidingPtr _nonColliding) const
{

  InterpolatedPtr untimedTrajectory;

  for (int i = 0; i < _tsr.size(); i++)
  {
    std::cout<<"Trying tsr number:"<<std::endl;
    std::cout<< i+1 <<std::endl;
    std::cout<<"Enter a number to begin planning"<<std::endl;
    int n;
    std::cin>>n;
    untimedTrajectory = this->planToTSR(_space, _bn, _tsr.at(i), _maxNumTrials, _timelimit, _nonColliding);
    if (untimedTrajectory)
    {
      break;
    }
  }

  if (!untimedTrajectory) 
    {
      throw std::runtime_error("Failed to find a solution with the passed tsr vector");
      return nullptr;
    }

  return untimedTrajectory;
}

InterpolatedPtr Planner::planToTSR(MetaSkeletonStateSpacePtr _space,
  dart::dynamics::BodyNodePtr _bn,
  const aikido::constraint::TSR& _tsr,
  int _maxNumTrials,
  double _timelimit,
  aikido::constraint::NonCollidingPtr _nonColliding) const
{
  using aikido::constraint::InverseKinematicsSampleable;
  using aikido::constraint::FiniteSampleable;
  using aikido::constraint::CyclicSampleable;
  using dart::dynamics::InverseKinematics;
  using aikido::constraint::TSR;
  using Clock = std::chrono::high_resolution_clock;
  using std::chrono::seconds;
  _timelimit = 1000*_timelimit;
  // Convert TSR constraint into IK constraint
  auto seedState = _space->getScopedStateFromMetaSkeleton();
  _space->getState(seedState);
  auto seed = std::make_shared<CyclicSampleable>(
    std::make_shared<FiniteSampleable>(_space, seedState));

  auto ik = InverseKinematics::create(_bn);

  auto tsr = std::make_shared<TSR>(_tsr);
  InverseKinematicsSampleable ikSampleable(_space, tsr, seed, ik,
    _maxNumTrials);

  auto generator = ikSampleable.createSampleGenerator();

  // Current state
  auto startState = _space->getScopedStateFromMetaSkeleton();

  // Goal state
  auto goalState = _space->createState();

  auto tStart = Clock::now();

  double timelimitPerSample = _timelimit/_maxNumTrials;

  for(int i = 0; generator->canSample() && _timelimit > 0; ++i)
  {
    double duration = std::chrono::duration_cast<std::chrono::milliseconds>(
      Clock::now() - tStart).count();
    _timelimit -= duration;
    tStart = Clock::now();
    // Sample from TSR
    bool sampled = generator->sample(goalState);
    if (!sampled){
      // Failed to sample from ikSampleable
      continue;
    }
    Eigen::VectorXd goal;
    _space->convertStateToPositions(goalState, goal);
    // Set to start state
    _space->setState(startState);
    auto traj = this->planToConfiguration(
      _space, goal, timelimitPerSample, _nonColliding);

    if (traj){
      return traj;
    }
    
  }
  return nullptr;

}

InterpolatedPtr Planner::planToConfiguration(MetaSkeletonStateSpacePtr _space,
                                          const Eigen::VectorXd& _goal,
                                          double _timelimit,
                                          NonCollidingPtr _nonColliding) const
{
  using aikido::constraint::createProjectableBounds;
  using aikido::constraint::createSampleableBounds;
  using aikido::constraint::createTestableBounds;
  using aikido::constraint::TestablePtr;
  using aikido::constraint::TestableIntersection;
  using aikido::distance::createDistanceMetric;
  using aikido::planner::ompl::planOMPL;
  using aikido::statespace::GeodesicInterpolator;
  using aikido::util::RNGWrapper;
  _timelimit = _timelimit/1000;
  auto startState = _space->getScopedStateFromMetaSkeleton();

  auto goalState = _space->createState();
  _space->convertPositionsToState(_goal, goalState);

  auto rng = make_unique<RNGWrapper<std::default_random_engine>>(0);

  auto selfNonColliding = getSelfCollisionConstraint(_space);

  // Make testable constraints for collision check
  std::vector<TestablePtr> constraints;

  if (_nonColliding == nullptr){
    constraints.push_back(selfNonColliding);
  }
  else
  {
    if (_nonColliding->getStateSpace() != _space)
    {
      throw std::runtime_error("NonColliding has incorrect statespace.");
    }

    constraints.reserve(2);

    constraints.emplace_back(selfNonColliding);

    constraints.emplace_back(_nonColliding);
  }

  auto testable = std::make_shared<TestableIntersection>(_space, constraints);

  aikido::planner::PlanningResult pResult;
  aikido::trajectory::InterpolatedPtr untimedTrajectory;

  untimedTrajectory = aikido::planner::planSnap(
      _space,
      startState,
      goalState,
      std::make_shared<GeodesicInterpolator>(_space),
      testable,
      pResult);

  if (untimedTrajectory)
    return untimedTrajectory;

  untimedTrajectory = planOMPL<ompl::geometric::RRTConnect>(
      startState,
      goalState,
      _space,
      std::make_shared<GeodesicInterpolator>(_space),
      createDistanceMetric(_space),
      createSampleableBounds(_space, std::move(rng)),
      testable,
      createTestableBounds(_space),
      createProjectableBounds(_space),
      _timelimit,
      this->_mCollisionResolution);

  return untimedTrajectory;
}

std::shared_ptr<aikido::constraint::NonColliding>
Planner::getSelfCollisionConstraint(MetaSkeletonStateSpacePtr _space) const
{
  using aikido::constraint::NonColliding;
  this->_robot->enableSelfCollisionCheck();
  this->_robot->disableAdjacentBodyCheck();
  for (const auto& bodyNodeName : this->_disableCollisions)
  {
   this->_robot->getBodyNode(bodyNodeName)->setCollidable(false);
  }
  auto collisionDetector = dart::collision::FCLCollisionDetector::create();
  // TODO: Switch to PRIMITIVE once this is fixed in DART.
  // collisionDetector->setPrimitiveShapeType(FCLCollisionDetector::PRIMITIVE);
  auto nonCollidingConstraint =
      std::make_shared<NonColliding>(_space, collisionDetector);
  nonCollidingConstraint->addSelfCheck(
      collisionDetector->createCollisionGroupAsSharedPtr(this->_robot.get()));
  return nonCollidingConstraint;
}

}