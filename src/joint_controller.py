#!/usr/bin/env python

import sys
import time
from itertools import count as Count

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from actionlib import SimpleActionServer as Server
import ai_engine.msg

from plant import PIDPlant as Plant

class JointController(object):
    def __init__(self, name, base):
        self.name = name
        self.base = base

        self.joints = rospy.get_param(self.base)
        self.count  = Count()
        self.plant  = Plant(self.joints['gain'], self.joints['rest'])

        self.state_pub  = rospy.Publisher(self.base+'/state',        JointState, queue_size=10)
        self.status_pub = rospy.Publisher(self.base+'/jctrl_status', String,     queue_size=10)

        status = "%s starting server at %s/\'ctrl\'" % (self.base, self.base)

        #rospy.loginfo(status)

        self.server = Server(self.base+'/ctrl', 
                             ai_engine.msg.joints_waypointAction, 
                             execute_cb=self.execute_callback,
                             auto_start=False)

        self.rate = 50.

    def run(self):
        self.server.start()

        rate = rospy.Rate(self.rate)

        while not rospy.is_shutdown():
            status = "%s is alive at %s" % (self.base, rospy.get_time())
            
            #rospy.loginfo(status)

            self.status_pub.publish(status)

            self.plant.update()

            state = JointState()

            state.header.seq   = next(self.count)
            state.header.stamp = rospy.Time.now()
            state.name         = self.joints['name']
            state.position     = self.plant.state()

            self.state_pub.publish(state)

            rate.sleep()

    def execute_callback(self, goal):
        status = "%s received goal %s at %s" % (self.base, str(goal.joints), rospy.get_time())
        
        #rospy.loginfo(status)

        self.plant.setpoint(goal.joints.position)

        while not rospy.is_shutdown() and \
              not self.plant.ready()  and \
              not self.server.is_preempt_requested():

            feedback = goal   

            feedback.joints.position = self.plant.state()

            self.server.publish_feedback(feedback)

            time.sleep(1./self.rate)

        if self.plant.ready():
            result = goal

            result.joints.position = self.plant.state()

            self.server.set_succeeded(result)


if __name__ == '__main__':
    rospy.init_node('joint_controller', anonymous=True)

    try:
        name = rospy.get_name()
        base = sys.argv[1]

        jctrl = JointController(name, base)

        jctrl.run()

        rospy.spin()

    except rospy.ROSInterruptException:
        pass