#!/usr/bin/env python

import sys
import time
from itertools import count as Count

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import JointState
import ai_engine.msg
from ai_engine.msg import JointTrajectory

import roslib
roslib.load_manifest('robot_comm')
import robot_comm.srv

class TrajectoryExecutorReal(object):
    def __init__(self, name, base):
        self.name = name
        self.base = base

        self.set_joints = rospy.ServiceProxy('/robot_SetJoints', 
                                             robot_comm.srv.robot_SetJoints)
        self.get_joints = rospy.ServiceProxy('/robot_GetJoints', 
                                             robot_comm.srv.robot_GetJoints)
        self.set_speed = rospy.ServiceProxy('/robot_SetSpeed', 
                                            robot_comm.srv.robot_SetSpeed)

        self.joints = rospy.get_param(self.base)
        self.count  = Count()

        self.trajectory_sub = rospy.Subscriber(self.base+'/trajectory', JointTrajectory, self.execute_callback)
        self.status_pub     = rospy.Publisher (self.base+'/texec_status',     String,          queue_size=10)

        self.rate = 1
        self.busy = False

    def run(self):
        rate = rospy.Rate(self.rate)

        while not rospy.is_shutdown():
            status = "%s is alive at %s" % (self.base, rospy.get_time())
            
            #rospy.loginfo(status)

            #self.status_pub.publish(status)

            rate.sleep()   

    def execute_callback(self, goal):
        status = "%s received trajectory at %s" % (self.base, rospy.get_time())

        #rospy.loginfo(status)

        if self.busy is True:
            status = "%s too busy at %s" % (self.base, rospy.get_time())

            #rospy.loginfo(status)

            return

        self.busy = True

        waypoints = goal.waypoints

        idx = 0

        while not rospy.is_shutdown() and \
              idx < len(waypoints):

            joints = waypoints[idx]

            joints.header.seq      = next(self.count)
            joints.header.stamp    = rospy.Time.now()

            #print >> sys.stderr, joints

            xgoal = ai_engine.msg.joints_waypointGoal(joints=joints)

            status = "%s sent goal %s at %s" % (self.base, xgoal, rospy.get_time())

            #rospy.loginfo(status)
            #print >> sys.stderr, 'aloha'

            self.set_joints(*[angle*180/3.1415962 for angle in waypoints[idx].position])

            results = self.get_joints()

            #status = "%s received result %s at %s" % (self.base, result, rospy.get_time())

            #rospy.loginfo(status)

            idx += 1

        self.busy = False


if __name__ == '__main__':
    rospy.init_node('Trajectory_executor_real', anonymous=True)

    try:
        name = rospy.get_name()
        base = sys.argv[1]

        texec = TrajectoryExecutorReal(name, base)

        texec.run()

        rospy.spin()

    except rospy.ROSInterruptException:
        pass